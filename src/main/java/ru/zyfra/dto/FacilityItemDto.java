package ru.zyfra.dto;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class FacilityItemDto {
    private Long id;
    private String name;
    private String description;
    private String type;
    private List<FacilityItemDto> children = new ArrayList<>();
    private Map<String, String> attributes = new HashMap<>();


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public List<FacilityItemDto> getChildren() {
        return children;
    }

    public Map<String, String> getAttributes() {
        return attributes;
    }
}
