package ru.zyfra.util.converter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.zyfra.domain.FacilityItem;
import ru.zyfra.dto.FacilityItemDto;
import ru.zyfra.service.FacilityItemService;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class FacilityItemConverter {

    private final FacilityItemService facilityItemService;

    @Autowired
    public FacilityItemConverter(FacilityItemService facilityItemService) {
        this.facilityItemService = facilityItemService;
    }

    public FacilityItemDto toDto(FacilityItem entity) {
        FacilityItemDto dto = new FacilityItemDto();

        dto.setId(entity.getId());
        dto.setName(entity.getName());
        dto.setType(entity.getType().name());
        dto.setDescription(entity.getDescription());

        List<FacilityItem> facilityItemChildren = facilityItemService.getAllByParentIs(entity);
        dto.getChildren().addAll(this.toDto(facilityItemChildren));

        entity.getAttributes().forEach(attribute -> dto.getAttributes().put(attribute.getAttribute().getCode(), attribute.getValue()));

        return dto;
    }

    private List<FacilityItemDto> toDto(List<FacilityItem> facilityItems) {
        return facilityItems.stream().map(this::toDto).collect(Collectors.toList());
    }
}
