package ru.zyfra.util.converter;

import ru.zyfra.domain.Attribute;
import ru.zyfra.dto.AttributeDto;

public class AttributeConverter {
    private AttributeConverter() {
    }

    public static AttributeDto toDto(Attribute entity) {
        AttributeDto dto = new AttributeDto();

        dto.setId(entity.getId());
        dto.setCode(entity.getCode());
        dto.setName(entity.getName());
        dto.setDescription(entity.getDescription());
        dto.setFacilityItemType(entity.getFacilityItemType().name());

        return dto;
    }
}
