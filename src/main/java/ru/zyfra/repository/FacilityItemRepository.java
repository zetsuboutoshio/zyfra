package ru.zyfra.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import ru.zyfra.domain.FacilityItem;

import java.util.List;
import java.util.Optional;

@Repository
public interface FacilityItemRepository extends CrudRepository<FacilityItem, Long> {
    List<FacilityItem> getAllByParentIsAndActiveTrue(FacilityItem parent);

    Optional<FacilityItem> findByIdAndActiveTrue(Long id);
}
