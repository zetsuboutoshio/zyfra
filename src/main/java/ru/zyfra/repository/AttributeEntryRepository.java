package ru.zyfra.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import ru.zyfra.domain.AttributeEntry;

@Repository
public interface AttributeEntryRepository extends CrudRepository<AttributeEntry, Long> {
}
