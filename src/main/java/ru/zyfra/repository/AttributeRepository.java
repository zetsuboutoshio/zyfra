package ru.zyfra.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import ru.zyfra.domain.Attribute;
import ru.zyfra.domain.FacilityItemType;

import java.util.Optional;

@Repository
public interface AttributeRepository extends CrudRepository<Attribute, Long> {
    Optional<Attribute> getByCodeAndFacilityItemTypeAndActiveTrue(String code, FacilityItemType type);
}
