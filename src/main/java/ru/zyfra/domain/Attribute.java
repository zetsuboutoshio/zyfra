package ru.zyfra.domain;

import javax.persistence.*;

@Entity
@Table(name = "attribute",
        uniqueConstraints = {@UniqueConstraint(columnNames = {"code", "facilityItemType"})})
public class Attribute {
    @Id
    @GeneratedValue
    private Long id;
    private String code;
    private String name;
    private String description;
    @Enumerated(value = EnumType.STRING)
    private FacilityItemType facilityItemType;
    private boolean active = true;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public FacilityItemType getFacilityItemType() {
        return facilityItemType;
    }

    public void setFacilityItemType(FacilityItemType facilityItemType) {
        this.facilityItemType = facilityItemType;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public boolean isActive() {
        return active;
    }
}
