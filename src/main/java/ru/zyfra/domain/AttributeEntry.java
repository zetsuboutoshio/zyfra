package ru.zyfra.domain;

import javax.persistence.*;

@Entity
@Table(name = "attribute_entry",
        uniqueConstraints = {@UniqueConstraint(columnNames = {"facility_item_id", "facility_attribute_id"})})
public class AttributeEntry {
    @Id
    @GeneratedValue
    private Long id;
    @ManyToOne
    @JoinColumn(name = "facility_item_id", nullable = false)
    private FacilityItem facilityItem;
    @ManyToOne
    @JoinColumn(name = "facility_attribute_id", nullable = false)
    private Attribute attribute;
    private String value;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Attribute getAttribute() {
        return attribute;
    }

    public void setAttribute(Attribute attribute) {
        this.attribute = attribute;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public FacilityItem getFacilityItem() {
        return facilityItem;
    }

    public void setFacilityItem(FacilityItem facilityItem) {
        this.facilityItem = facilityItem;
    }

    @Override
    public String toString() {
        return attribute.getName() + ": " + value;
    }
}
