package ru.zyfra.domain;

import java.util.Arrays;
import java.util.List;

public enum FacilityItemType {
    SENSOR,
    FACILITY(SENSOR),
    AREA(FACILITY, SENSOR),
    DEPARTMENT(AREA),
    FACTORY(DEPARTMENT);

    List<FacilityItemType> listOfPossibleChildren;

    FacilityItemType(FacilityItemType... children) {
        listOfPossibleChildren = Arrays.asList(children);
    }

    public boolean canByParentOf(FacilityItemType type) {
        return listOfPossibleChildren.contains(type);
    }
}
