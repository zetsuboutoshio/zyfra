package ru.zyfra.domain;

import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "facility_item")
public class FacilityItem {
    @Id
    @GeneratedValue
    private Long id;
    @Enumerated(value = EnumType.STRING)
    private FacilityItemType type;
    private String name;
    private String description;
    @ManyToOne
    @JoinColumn(name = "parent_id")
    private FacilityItem parent;
    @OneToMany(mappedBy = "facilityItem")
    @LazyCollection(value = LazyCollectionOption.TRUE)
    private List<AttributeEntry> attributes;
    private boolean active = true;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public FacilityItemType getType() {
        return type;
    }

    public void setType(FacilityItemType type) {
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public FacilityItem getParent() {
        return parent;
    }

    public void setParent(FacilityItem parent) {
        this.parent = parent;
    }

    public List<AttributeEntry> getAttributes() {
        return attributes;
    }

    public void setAttributes(List<AttributeEntry> attributes) {
        this.attributes = attributes;
    }

    public FacilityItem() {
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public boolean isActive() {
        return active;
    }
}
