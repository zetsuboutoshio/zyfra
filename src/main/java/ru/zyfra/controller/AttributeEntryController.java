package ru.zyfra.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import ru.zyfra.service.FacilityItemService;

@RestController

@RequestMapping("/api")
public class AttributeEntryController {

    private final FacilityItemService facilityItemService;

    @Autowired
    public AttributeEntryController(FacilityItemService facilityItemService) {
        this.facilityItemService = facilityItemService;
    }

    @PutMapping("/facility/attribute")
    public void createAttributeValueForItem(
            @RequestParam Long itemId,
            @RequestParam String attributeCode,
            @RequestParam String value
    ) throws Exception {
        facilityItemService.setAttributeValue(itemId, attributeCode, value);
    }
}
