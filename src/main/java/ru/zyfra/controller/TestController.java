package ru.zyfra.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.zyfra.domain.Attribute;
import ru.zyfra.domain.FacilityItem;
import ru.zyfra.domain.FacilityItemType;
import ru.zyfra.service.AttributeService;
import ru.zyfra.service.FacilityItemService;

import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.Map;

@RestController
public class TestController {

    private final FacilityItemService facilityItemService;
    private final AttributeService attributeService;

    @Autowired
    public TestController(FacilityItemService facilityItemService, AttributeService attributeService) {
        this.facilityItemService = facilityItemService;
        this.attributeService = attributeService;
    }

    @GetMapping
    void init() throws Exception {
        initMeta();
        initAttrMeta();
        initAttrValues();
    }

    private void initAttrValues() throws Exception {
        String jsonString = getJsonFromFile("/init/attrs.json");

        List<Map> attrEntries = new ObjectMapper().readerFor(List.class).readValue(jsonString);

        for (Map attr : attrEntries) {
            Long id = ((Integer) attr.get("id")).longValue();
            String code = (String) attr.get("code");
            String value = (String) attr.get("value");

            facilityItemService.setAttributeValue(id, code, value);
        }
    }

    private String getJsonFromFile(String path) throws IOException {
        InputStream in = getClass().getResourceAsStream(path);
        byte[] targetArray = IOUtils.toByteArray(in);

        return new String(targetArray, StandardCharsets.UTF_8);
    }

    private void initAttrMeta() throws IOException {
        String jsonString = getJsonFromFile("/init/attr_meta.json");

        List<Map> attrs = new ObjectMapper().readerFor(List.class).readValue(jsonString);

        for (Map attr : attrs) {
            Attribute attribute = new Attribute();

            attribute.setId(((Integer) attr.get("id")).longValue());
            attribute.setName((String) attr.get("name"));
            attribute.setCode((String) attr.get("code"));
            attribute.setFacilityItemType(FacilityItemType.valueOf((String) attr.get("facilityItemType")));
            attribute.setDescription((String) attr.get("description"));

            attributeService.updateAttribute(attribute);
        }
    }

    private void initMeta() throws Exception {
        String jsonString = getJsonFromFile("/init/meta.json");

        List<Map> items = new ObjectMapper().readerFor(List.class).readValue(jsonString);

        for (Map item : items) {
            FacilityItem facilityItem = new FacilityItem();

            facilityItem.setId(((Integer) item.get("id")).longValue());
            facilityItem.setName((String) item.get("name"));
            facilityItem.setType(FacilityItemType.valueOf((String) item.get("type")));
            facilityItem.setDescription((String) item.get("description"));

            Object parentIdObject = item.get("parentId");
            Long parentId = null;

            if (null != parentIdObject) {
                parentId = ((Integer) parentIdObject).longValue();
            }

            facilityItemService.updateItem(facilityItem, parentId);
        }
    }
}
