package ru.zyfra.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.zyfra.domain.Attribute;
import ru.zyfra.domain.FacilityItemType;
import ru.zyfra.dto.AttributeDto;
import ru.zyfra.service.AttributeService;
import ru.zyfra.util.converter.AttributeConverter;

@RestController
@RequestMapping("/api")
public class AttributeController {

    private final AttributeService attributeService;

    @Autowired
    public AttributeController(AttributeService attributeService) {
        this.attributeService = attributeService;
    }

    @GetMapping("/attribute")
    public AttributeDto getAttribute(@RequestParam("code") String code, @RequestParam("type") FacilityItemType type) throws Exception {
        return AttributeConverter.toDto(attributeService.getByCodeAndType(code, type));
    }

    @DeleteMapping("/attribute")
    public void deleteAttribute(@RequestParam("code") String code, @RequestParam("type") FacilityItemType type) throws Exception {
        attributeService.disable(code, type);
    }

    @PutMapping("/attribute")
    public void createNewAttribute(
            @RequestParam String name,
            @RequestParam String code,
            @RequestParam(required = false) String description,
            @RequestParam FacilityItemType type) {
        Attribute attribute = new Attribute();

        attribute.setName(name);
        attribute.setCode(code);
        attribute.setDescription(description);
        attribute.setFacilityItemType(type);

        attributeService.updateAttribute(attribute);
    }
}
