package ru.zyfra.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.zyfra.domain.FacilityItem;
import ru.zyfra.domain.FacilityItemType;
import ru.zyfra.dto.FacilityItemDto;
import ru.zyfra.service.FacilityItemService;
import ru.zyfra.util.converter.FacilityItemConverter;

@RestController
@RequestMapping("api/facility")
public class FacilityItemController {

    private final FacilityItemService facilityItemService;
    private final FacilityItemConverter facilityItemConverter;

    @Autowired
    public FacilityItemController(FacilityItemService facilityItemService, FacilityItemConverter facilityItemConverter) {
        this.facilityItemService = facilityItemService;
        this.facilityItemConverter = facilityItemConverter;
    }

    @GetMapping("/item/{id}")
    public FacilityItemDto getFacilityItem(@PathVariable long id) throws Exception {
        return facilityItemConverter.toDto(facilityItemService.getFacilityItem(id));
    }

    @DeleteMapping("/item/{id}")
    public void deleteItem(@PathVariable long id) throws Exception {
        facilityItemService.delete(id);
    }

    @PutMapping("/item")
    public void createFacilityItem(
            @RequestParam String name,
            @RequestParam(required = false) String description,
            @RequestParam FacilityItemType type,
            @RequestParam(required = false) Long parentId
    ) throws Exception {
        FacilityItem facilityItem = new FacilityItem();

        facilityItem.setType(type);
        facilityItem.setName(name);
        facilityItem.setDescription(description);

        facilityItemService.updateItem(facilityItem, parentId);
    }
}
