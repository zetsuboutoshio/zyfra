package ru.zyfra.service;

import ru.zyfra.domain.AttributeEntry;

public interface AttributeEntryService {
    Long updateEntry(AttributeEntry attributeEntry);
}
