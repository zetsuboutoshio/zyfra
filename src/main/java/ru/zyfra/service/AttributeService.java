package ru.zyfra.service;

import ru.zyfra.domain.Attribute;
import ru.zyfra.domain.FacilityItemType;

public interface AttributeService {
    Long updateAttribute(Attribute attribute);

    Attribute getByCodeAndType(String code, FacilityItemType type) throws Exception;

    void disable(String code, FacilityItemType type) throws Exception;
}
