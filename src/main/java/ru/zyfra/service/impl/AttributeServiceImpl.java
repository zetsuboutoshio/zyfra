package ru.zyfra.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.zyfra.domain.Attribute;
import ru.zyfra.domain.FacilityItemType;
import ru.zyfra.repository.AttributeRepository;
import ru.zyfra.service.AttributeService;

import java.util.Optional;

@Service
public class AttributeServiceImpl implements AttributeService {

    private final AttributeRepository attributeRepository;

    private AttributeService self;

    @Autowired
    public void setSelf(AttributeService attributeService) {
        this.self = attributeService;
    }

    @Autowired
    public AttributeServiceImpl(AttributeRepository attributeRepository) {
        this.attributeRepository = attributeRepository;
    }

    @Override
    public Long updateAttribute(Attribute attribute) {
        attribute = attributeRepository.save(attribute);
        return attribute.getId();
    }

    @Override
    public Attribute getByCodeAndType(String code, FacilityItemType type) throws Exception {
        Optional<Attribute> optionalFacilityAttribute = attributeRepository.getByCodeAndFacilityItemTypeAndActiveTrue(code, type);
        if (!optionalFacilityAttribute.isPresent()) {
            throw new Exception(String.format("Wrong attribute code - %s", code));
        }
        return optionalFacilityAttribute.get();
    }

    @Override
    @Transactional
    public void disable(String code, FacilityItemType type) throws Exception {
        Attribute attribute = self.getByCodeAndType(code, type);
        attribute.setActive(false);
        self.updateAttribute(attribute);
    }

}
