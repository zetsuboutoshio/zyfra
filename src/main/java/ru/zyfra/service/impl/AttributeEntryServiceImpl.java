package ru.zyfra.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.zyfra.domain.AttributeEntry;
import ru.zyfra.repository.AttributeEntryRepository;
import ru.zyfra.service.AttributeEntryService;

@Service
public class AttributeEntryServiceImpl implements AttributeEntryService {

    private final AttributeEntryRepository attributeEntryRepository;

    @Autowired
    public AttributeEntryServiceImpl(AttributeEntryRepository attributeEntryRepository) {
        this.attributeEntryRepository = attributeEntryRepository;
    }

    @Override
    public Long updateEntry(AttributeEntry attributeEntry) {
        attributeEntry = attributeEntryRepository.save(attributeEntry);
        return attributeEntry.getId();
    }
}
