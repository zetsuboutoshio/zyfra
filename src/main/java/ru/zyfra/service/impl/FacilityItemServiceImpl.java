package ru.zyfra.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.zyfra.domain.Attribute;
import ru.zyfra.domain.AttributeEntry;
import ru.zyfra.domain.FacilityItem;
import ru.zyfra.repository.FacilityItemRepository;
import ru.zyfra.service.AttributeEntryService;
import ru.zyfra.service.AttributeService;
import ru.zyfra.service.FacilityItemService;

import java.util.List;
import java.util.Optional;

@Service
public class FacilityItemServiceImpl implements FacilityItemService {

    private final FacilityItemRepository facilityItemRepository;
    private final AttributeService attributeService;
    private final AttributeEntryService attributeEntryService;
    private FacilityItemService self;

    @Autowired
    public FacilityItemServiceImpl(
            FacilityItemRepository facilityItemRepository,
            AttributeService attributeService,
            AttributeEntryService attributeEntryService
    ) {
        this.facilityItemRepository = facilityItemRepository;
        this.attributeService = attributeService;
        this.attributeEntryService = attributeEntryService;

    }

    @Autowired
    public void setSelf(FacilityItemService self) {
        this.self = self;
    }

    @Override
    public FacilityItem getFacilityItem(Long id) throws Exception {
        Optional<FacilityItem> optionalFacilityItem = facilityItemRepository.findByIdAndActiveTrue(id);
        if (!optionalFacilityItem.isPresent()) {
            throw new Exception(String.format("Wrong item id - %s", id));
        }
        return optionalFacilityItem.get();
    }

    @Override
    public Long updateItem(FacilityItem facilityItem, Long parentId) throws Exception {
        if (null != parentId) {
            FacilityItem parent = getFacilityItem(parentId);

            if (null == parent) {
                throw new Exception("Wrong parent id");
            }

            if (parent.getType().canByParentOf(facilityItem.getType())) {
                facilityItem.setParent(parent);
            } else {
                throw new Exception(String.format("%s can't ba parent of %s", parent.getType(), facilityItem.getType()));
            }
        }

        facilityItem = facilityItemRepository.save(facilityItem);
        return facilityItem.getId();
    }

    @Override
    public List<FacilityItem> getAllByParentIs(FacilityItem parent) {
        return facilityItemRepository.getAllByParentIsAndActiveTrue(parent);
    }

    @Override
    public void setAttributeValue(Long id, String attributeCode, String value) throws Exception {
        FacilityItem item = getFacilityItem(id);

        Attribute attribute = attributeService.getByCodeAndType(attributeCode, item.getType());

        if (item.getType() != attribute.getFacilityItemType()) {
            throw new Exception(String.format("Incompatible types of item (%s) and attribute (%s)", item.getType(), attribute.getFacilityItemType()));
        }

        AttributeEntry attributeEntry = new AttributeEntry();
        attributeEntry.setAttribute(attribute);
        attributeEntry.setFacilityItem(item);
        attributeEntry.setValue(value);

        attributeEntryService.updateEntry(attributeEntry);
    }

    @Override
    public void delete(Long id) throws Exception {
        FacilityItem facilityItem = getFacilityItem(id);
        facilityItem.setActive(false);
        self.updateItem(facilityItem);
    }
}
