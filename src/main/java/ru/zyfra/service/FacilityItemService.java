package ru.zyfra.service;

import ru.zyfra.domain.FacilityItem;

import java.util.List;

public interface FacilityItemService {
    FacilityItem getFacilityItem(Long id) throws Exception;

    Long updateItem(FacilityItem facilityItem, Long parentId) throws Exception;

    default Long updateItem(FacilityItem facilityItem) throws Exception {
        return updateItem(facilityItem, null);
    }

    List<FacilityItem> getAllByParentIs(FacilityItem parent);

    void setAttributeValue(Long id, String attributeCode, String value) throws Exception;

    void delete(Long id) throws Exception;
}
